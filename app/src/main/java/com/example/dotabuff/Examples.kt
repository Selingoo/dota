package com.example.dotabuff

fun main() {
    foo("qweqweqwe") // "qweqweqwe" to "qweqweqwe"             !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! как правильно говорить , я создаю элеменет от foo?
    foo(1243132) // 1243132 to 1243132
    foo(true) // true to true

//    foo2(12345) // не подходит под ограничение
//    foo2("wert") // не подходит под ограничение
    foo2(SeregaUser())
    foo2(MaxUser())
}

// <T> - Дженерик
fun <T> foo(item: T): Pair<T, T> {
    return item to item
}

fun <T : BaseUser> foo2(item: T): Pair<T, T> {
    return item to item
}

open class BaseUser()

class SeregaUser() : BaseUser()
class MaxUser() : BaseUser()