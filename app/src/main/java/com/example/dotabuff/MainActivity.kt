package com.example.dotabuff

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.dotabuff.model.base.BaseAgilityHero
import com.example.dotabuff.model.base.BaseHero
import com.example.dotabuff.model.base.BaseIntelligenceHero
import com.example.dotabuff.model.base.BaseStrangeHero
import com.example.dotabuff.model.hero.*

class MainActivity : AppCompatActivity(), HeroItemClick {
    private val adapterHero = HeroRecyclerViewAdapter(this)

    //todo registerForActivityResult = подписываем текущий активити на получение результата из активити которое мы открываем с помощью launch
    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            val value =  intent?.getStringExtra("RESULT_TEXT")
            Toast.makeText(applicationContext, value, Toast.LENGTH_SHORT).show ()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hero)
        listHeroInfo()
    }

    override fun infoClick(hero: BaseHero) {
        val activityHero = Intent(this, SecondActivity::class.java)
        activityHero.putExtra("keyName", hero)
        startForResult.launch(activityHero)
    }

    override fun deleteClick(hero: BaseHero) {
        val listHero = adapterHero.currentList.filterNot { hero.name == it.name }
        adapterHero.submitList(listHero)

//        val listHero:MutableList<BaseHero> = adapterHero.currentList
//        listHero.remove(hero)
//        adapterHero.submitList(listHero)
    }

    private fun listHeroInfo() {
        val recyclerHeroViewNew = findViewById<RecyclerView>(R.id.recyclerHeroViewNew)
        recyclerHeroViewNew.adapter = adapterHero

        val heroListView = listOf(Axe(), DrowRanger(), Io(), AntiMage(), OgreMage(), Zeus())
        adapterHero.submitList(heroListView)

        val sortButton = findViewById<Button>(R.id.sortHero)
        val sortStrangeButton = findViewById<Button>(R.id.sortHeroStr)
        val sortAgilityButton = findViewById<Button>(R.id.sortHeroAgl)
        val sortIntelligenceButton = findViewById<Button>(R.id.sortHeroInt)

        sortButton.setOnClickListener {
            val heroListViewName = heroListView.sortedBy { it.name }
            adapterHero.submitList(heroListViewName)
        }

        sortStrangeButton.setOnClickListener {
            val heroListViewStrange = heroListView.filter { it is BaseStrangeHero }
            adapterHero.submitList(heroListViewStrange)
        }

        sortAgilityButton.setOnClickListener {
            val heroListViewAgility = heroListView.filter { it is BaseAgilityHero }
            adapterHero.submitList(heroListViewAgility)
        }

        sortIntelligenceButton.setOnClickListener {
            val heroListViewIntelligence = heroListView.filter { it is BaseIntelligenceHero }
            adapterHero.submitList(heroListViewIntelligence)
        }
    }
}