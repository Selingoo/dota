package com.example.dotabuff.model.hero

import com.example.dotabuff.model.base.AttackType
import com.example.dotabuff.model.base.BaseAgilityHero
import com.example.dotabuff.model.base.BaseAttack
import kotlinx.android.parcel.Parcelize

@Parcelize
class AntiMage(
    override val name: String = "Anti-mage",
    override val image: String = "https://avavatar.ru/images/avatars/32/avatar_I9Mpu2y7AEEx0GAr.jpg",
//    override val image: String = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdGvKuoWZoV3pcG7N4QYTBO6kAy_RcY3wqDu714w4f3P5oSyZTsml80FotTuL94tGwOJfOTV_eVxw&usqp=CAU",
    override val hp: Int = 400,
    override val mp: Int = 200,
    override val hpRegeneration: Double = 1.8,
    override val mpRegeneration: Double = 1.2,
    override val strange: Int = 15,
    override val agility: Int = 23,
    override val intelligence: Int = 17,
    override val levelUpPlusStrange: Double = 1.7,
    override val levelUpPlusAgility: Double = 2.7,
    override val levelUpPlusIntelligence: Double = 1.6,
    override var damage: Int = 34,
    override val attackSpeed: Int = 100,
    override val speed: Int = 310,
    override val armor: Int = 2,
    override var levelHero: Int = 1,

    override val rangeType: AttackType = AttackType.MILLI_HERO,
    override val rangeDistance: Int = 150
) : BaseAgilityHero(), BaseAttack
