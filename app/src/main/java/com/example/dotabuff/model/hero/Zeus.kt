package com.example.dotabuff.model.hero

import com.example.dotabuff.model.base.AttackType
import com.example.dotabuff.model.base.BaseAttack
import com.example.dotabuff.model.base.BaseIntelligenceHero
import kotlinx.android.parcel.Parcelize

@Parcelize
class Zeus(
    override val name: String = "Zeus",
    override val image: String = "http://sun9-19.userapi.com/s/v1/ig2/1VbnU8xrOie66qOqtpDl5jnG8fVtWqGhyvddR1tr4HLIX02NXDR7xmgIM7qbIH9BRWWQ7P1WVdaUK8GtIfKoVxGu.jpg?size=200x0&quality=96&crop=483,63,953,953&ava=1",
//    override val image: String = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdGvKuoWZoV3pcG7N4QYTBO6kAy_RcY3wqDu714w4f3P5oSyZTsml80FotTuL94tGwOJfOTV_eVxw&usqp=CAU",
    override val hp: Int = 450,
    override val mp: Int = 400,
    override val hpRegeneration: Double = 1.3,
    override val mpRegeneration: Double = 1.8,
    override val strange: Int = 18,
    override val agility: Int = 16,
    override val intelligence: Int = 26,
    override val levelUpPlusStrange: Double = 2.4,
    override val levelUpPlusAgility: Double = 2.1,
    override val levelUpPlusIntelligence: Double = 3.6,
    override var damage: Int = 36,
    override val attackSpeed: Int = 100,
    override val speed: Int = 300,
    override val armor: Int = 2,
    override var levelHero: Int = 1,

    override val rangeType: AttackType = AttackType.RANGE_HERO,
    override val rangeDistance: Int = 500
) : BaseIntelligenceHero(), BaseAttack