package com.example.dotabuff.model.base

enum class AttackType { MILLI_HERO, RANGE_HERO }

interface BaseAttack {

    val rangeType: AttackType

    val rangeDistance: Int
}

