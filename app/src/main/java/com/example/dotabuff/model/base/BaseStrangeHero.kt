package com.example.dotabuff.model.base

abstract class BaseStrangeHero() : BaseHero {

    override fun getHeroDamage() = damage + getHeroStrange()

}