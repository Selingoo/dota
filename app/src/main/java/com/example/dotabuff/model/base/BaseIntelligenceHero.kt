package com.example.dotabuff.model.base

abstract class BaseIntelligenceHero() : BaseHero {

    override fun getHeroDamage() = damage + getHeroIntelligence()

}