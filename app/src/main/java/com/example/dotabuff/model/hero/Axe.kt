package com.example.dotabuff.model.hero

import android.os.Parcelable
import com.example.dotabuff.model.base.AttackType
import com.example.dotabuff.model.base.BaseAttack
import com.example.dotabuff.model.base.BaseStrangeHero
import kotlinx.android.parcel.Parcelize

@Parcelize
class Axe(
    override val name: String = "Axe",
    override val image: String = "https://avavatar.ru/images/avatars/16/avatar_i7prQPXHULSuQcrU.jpg",
//    override val image: String = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdGvKuoWZoV3pcG7N4QYTBO6kAy_RcY3wqDu714w4f3P5oSyZTsml80FotTuL94tGwOJfOTV_eVxw&usqp=CAU",
    override val hp: Int = 500,
    override val mp: Int = 200,
    override val hpRegeneration: Double = 1.5,
    override val mpRegeneration: Double = 1.4,
    override val strange: Int = 25,
    override val agility: Int = 19,
    override val intelligence: Int = 16,
    override val levelUpPlusStrange: Double = 1.5,
    override val levelUpPlusAgility: Double = 2.4,
    override val levelUpPlusIntelligence: Double = 1.4,
    override var damage: Int = 42,
    override val attackSpeed: Int = 110,
    override val speed: Int = 315,
    override val armor: Int = 2,
    override var levelHero: Int = 1,

    override val rangeType: AttackType = AttackType.MILLI_HERO,
    override val rangeDistance: Int = 150
) : BaseStrangeHero(), BaseAttack


