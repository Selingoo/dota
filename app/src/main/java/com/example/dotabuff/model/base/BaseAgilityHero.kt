package com.example.dotabuff.model.base

abstract class BaseAgilityHero() : BaseHero {

    override fun getHeroDamage() = damage + getHeroAgility()
}