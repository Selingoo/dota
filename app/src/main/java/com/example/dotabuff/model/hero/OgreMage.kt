package com.example.dotabuff.model.hero

import android.os.Parcelable
import com.example.dotabuff.model.base.AttackType
import com.example.dotabuff.model.base.BaseAttack
import com.example.dotabuff.model.base.BaseIntelligenceHero
import kotlinx.android.parcel.Parcelize

@Parcelize
class OgreMage(
    override val name: String = "Ogre-mage",
    override val image: String = "https://orcz.com/images/thumb/0/02/Dota2ogremagi.jpg/400px-Dota2ogremagi.jpg",
//    override val image: String = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdGvKuoWZoV3pcG7N4QYTBO6kAy_RcY3wqDu714w4f3P5oSyZTsml80FotTuL94tGwOJfOTV_eVxw&usqp=CAU",
    override val hp: Int = 500,
    override val mp: Int = 300,
    override val hpRegeneration: Double = 3.5,
    override val mpRegeneration: Double = 1.6,
    override val strange: Int = 23,
    override val agility: Int = 20,
    override val intelligence: Int = 26,
    override val levelUpPlusStrange: Double = 1.5,
    override val levelUpPlusAgility: Double = 2.4,
    override val levelUpPlusIntelligence: Double = 1.4,
    override var damage: Int = 40,
    override val attackSpeed: Int = 100,
    override val speed: Int = 320,
    override val armor: Int = 6,
    override var levelHero: Int = 1,

    override val rangeType: AttackType = AttackType.MILLI_HERO,
    override val rangeDistance: Int = 150
) : BaseIntelligenceHero(), BaseAttack
