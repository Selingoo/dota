package com.example.dotabuff.model.hero

import com.example.dotabuff.model.base.AttackType
import com.example.dotabuff.model.base.BaseAgilityHero
import com.example.dotabuff.model.base.BaseAttack
import kotlinx.android.parcel.Parcelize

@Parcelize
class DrowRanger(
    override val name: String = "Drow-ranger",
    override val image: String = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdGvKuoWZoV3pcG7N4QYTBO6kAy_RcY3wqDu714w4f3P5oSyZTsml80FotTuL94tGwOJfOTV_eVxw&usqp=CAU",
    override val hp: Int = 400,
    override val mp: Int = 300,
    override val hpRegeneration: Double = 1.5,
    override val mpRegeneration: Double = 1.4,
    override val strange: Int = 15,
    override val agility: Int = 19,
    override val intelligence: Int = 14,
    override val levelUpPlusStrange: Double = 1.5,
    override val levelUpPlusAgility: Double = 2.4,
    override val levelUpPlusIntelligence: Double = 1.4,
    override var damage: Int = 45,
    override val attackSpeed: Int = 120,
    override val speed: Int = 295,
    override val armor: Int = 2,
    override var levelHero: Int = 1,

    override val rangeType: AttackType = AttackType.RANGE_HERO,
    override val rangeDistance: Int = 600
) : BaseAgilityHero(), BaseAttack
