package com.example.dotabuff.model.base

import android.os.Parcelable


interface BaseHero : Parcelable {
    // hp + mp
    val name: String
    val image: String

    val hp: Int
    val mp: Int
    val hpRegeneration: Double
    val mpRegeneration: Double

    // attrs
    val strange: Int
    val agility: Int
    val intelligence: Int

    val levelUpPlusStrange: Double
    val levelUpPlusAgility: Double
    val levelUpPlusIntelligence: Double

    // attack
    var damage: Int
    val attackSpeed: Int

    // absolution
    val speed: Int
    val armor: Int
    var levelHero: Int

    fun lvlUp() {
        levelHero += 1
    }

    // attrs ---------------------------------------------------------------------------------------

    fun getHeroStrange(): Int = strange + (levelHero * levelUpPlusStrange).toInt()

    fun getHeroAgility(): Int = agility + (levelHero * levelUpPlusAgility).toInt()

    fun getHeroIntelligence(): Int = intelligence + (levelHero * levelUpPlusIntelligence).toInt()

    // attrs end -----------------------------------------------------------------------------------

    open fun getHeroDamage(): Int = damage

    fun getHeroArmor() = armor + (getHeroAgility() * 0.1).toInt()

    fun getHeroAttackSpeed() = attackSpeed + getHeroAgility()

    fun getHeroHp(): Int = hp + (getHeroStrange() * 20)

    fun getHeroMp(): Int = mp + (getHeroIntelligence() * 20)

    fun getHeroMoveSpeed(): Int = speed
}