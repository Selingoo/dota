package com.example.dotabuff

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.dotabuff.model.base.BaseHero


class SecondActivity : AppCompatActivity() {
    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra("RESULT_TEXT", "я нажал назад")
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val hero = intent.getParcelableExtra<BaseHero>("keyName")

        val strangeTv = findViewById<TextView>(R.id.strangeTv2)
        val agilityTv = findViewById<TextView>(R.id.agilityTv2)
        val intelligenceTv = findViewById<TextView>(R.id.intelligenceSee)

        val hpTv = findViewById<TextView>(R.id.hpTv2)
        val mpTv = findViewById<TextView>(R.id.mpTv2)
        val hpRegen = findViewById<TextView>(R.id.hpRegen)
        val mpRegen = findViewById<TextView>(R.id.mpRegen)

        val attackSpeed = findViewById<TextView>(R.id.attackSpeed)
        val armor = findViewById<TextView>(R.id.armor)
        val damage = findViewById<TextView>(R.id.attack)
        val lvl = findViewById<TextView>(R.id.lvl)

     
//        var image: String =
//            "https://upload.wikimedia.org/wikipedia/ru/9/9d/%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_DotA_2.jpg"
//        image = findViewById<View>(R.id.image).viewTreeObserver.toString()
//        Glide.with(baseContext).load(image).into(image)

        strangeTv.text = "HP = ${hero?.getHeroStrange().toString()}"
        agilityTv.text = "Agility = ${hero?.getHeroAgility().toString()}"
        intelligenceTv.text = "HP = ${hero?.getHeroIntelligence().toString()}"

        hpTv.text = "HP = ${hero?.getHeroHp().toString()}"
        mpTv.text = "MP = ${hero?.getHeroMp().toString()}"
        hpRegen.text = "HP regen = ${hero?.hpRegeneration.toString()}"
        mpRegen.text = "MP regen = ${hero?.mpRegeneration.toString()}"

        attackSpeed.text = "Attack Speed = ${hero?.attackSpeed.toString()}"
        damage.text = "Attack  = ${hero?.getHeroDamage().toString()}"
        armor.text = "armor  = ${hero?.getHeroArmor().toString()}"

        val linkHero = findViewById<TextView>(R.id.nameText)
        val speedHero = findViewById<TextView>(R.id.speedText2)
        linkHero.text = hero?.name
//        speedHero.text = hero?.speed.toString()
        speedHero.text = "Move Speed = ${hero?.getHeroMoveSpeed().toString()}"
        lvl.text = "lvl = ${hero?.levelHero.toString()}"

        val actionBar = supportActionBar
        if (actionBar != null) actionBar.title = "Герой не изучен"
        actionBar?.setDisplayHomeAsUpEnabled(true)

        // todo вернуться назад на MainActivity и показать там Toast и хмл секонд активити сделать красивым
        val backButton = findViewById<Button>(R.id.backButton)
        backButton.setOnClickListener {
            val intent = Intent()
            intent.putExtra("RESULT_TEXT", "Герой изучен")
            setResult(RESULT_OK, intent)
            finish()
        }
    }
}

//private fun Any.into(image: String) {
//        "https://upload.wikimedia.org/wikipedia/ru/9/9d/%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_DotA_2.jpg"
//}
