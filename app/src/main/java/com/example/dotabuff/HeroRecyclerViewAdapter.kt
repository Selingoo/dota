package com.example.dotabuff

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.dotabuff.model.base.BaseHero

class HeroRecyclerViewAdapter(private val heroItemClick: HeroItemClick) :
    ListAdapter<BaseHero, HeroRecyclerViewAdapter.HeroViewHolderNew>(HeroDiffUtils()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolderNew {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_hero, parent, false)
        return HeroViewHolderNew(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HeroViewHolderNew, position: Int) {
        holder.bind(getItem(position))
//        holder.bind(currentList[position]) // todo the same
    }

    inner class HeroViewHolderNew(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var infoButton: Button? = null
        var deleteButton: Button? = null

        var heroImage: ImageView? = null
        var heroTexts: TextView? = null
        var strangeTv: TextView? = null
        var agilityTv: TextView? = null
        var inteligenceTv: TextView? = null
        var hpTv: TextView? = null
        var mpTv: TextView? = null
        var moveSpeedTv: TextView? = null

        fun bind(itemHero: BaseHero) {
            heroImage = itemView.findViewById(R.id.heroImage)
            heroTexts = itemView.findViewById(R.id.heroText)
            strangeTv = itemView.findViewById(R.id.strangeTv)
            agilityTv = itemView.findViewById(R.id.agilityTv)
            inteligenceTv = itemView.findViewById(R.id.inteligenceTv)
            hpTv = itemView.findViewById(R.id.hpTv)
            mpTv = itemView.findViewById(R.id.mpTv)
            moveSpeedTv = itemView.findViewById(R.id.moveSpeedTv)
            infoButton = itemView.findViewById(R.id.infoButton)
            deleteButton = itemView.findViewById(R.id.deleteButton)

            infoButton?.setOnClickListener { heroItemClick.infoClick(itemHero) }
            deleteButton?.setOnClickListener { heroItemClick.deleteClick(itemHero) }

            heroTexts?.text = itemHero.name
            strangeTv?.text = "hero strange = ${itemHero.getHeroStrange()}"
            agilityTv?.text = "hero agility = ${itemHero.getHeroAgility()}"
            inteligenceTv?.text = "hero intelligence = ${itemHero.getHeroIntelligence()}"
            hpTv?.text = "HP= ${itemHero.getHeroHp()}"
            mpTv?.text = "MP = ${itemHero.getHeroMp()}"
            moveSpeedTv?.text = "move speed = ${itemHero.getHeroMoveSpeed()}"
            Glide.with(itemView.context).load(itemHero.image).into(heroImage!!)
        }
    }

    class HeroDiffUtils : DiffUtil.ItemCallback<BaseHero>() {
        override fun areItemsTheSame(oldItem: BaseHero, newItem: BaseHero) =
            oldItem.hashCode() == newItem.hashCode()

        override fun areContentsTheSame(oldItem: BaseHero, newItem: BaseHero) =
            oldItem.name == newItem.name
    }
}


interface HeroItemClick {
    fun infoClick(hero: BaseHero)
    fun deleteClick(hero: BaseHero)
}


